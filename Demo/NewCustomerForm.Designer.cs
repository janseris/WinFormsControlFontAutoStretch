﻿
namespace JanSeris.FontAutoStretch.Demo
{
    partial class NewCustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.employers = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.relationshipStatuses = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cities = new System.Windows.Forms.ComboBox();
            this.age = new System.Windows.Forms.NumericUpDown();
            this.name = new System.Windows.Forms.TextBox();
            this.citySections = new System.Windows.Forms.ListBox();
            this.smokingHabits = new System.Windows.Forms.CheckedListBox();
            this.drinkingHabits = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.age)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.employers, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.relationshipStatuses, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.cities, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.age, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.name, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.citySections, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.smokingHabits, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.drinkingHabits, 1, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.141813F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.141839F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.141813F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.141839F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28654F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28654F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28654F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28654F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28654F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(496, 483);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // employers
            // 
            this.employers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.employers.FormattingEnabled = true;
            this.employers.Location = new System.Drawing.Point(251, 105);
            this.employers.Name = "employers";
            this.employers.Size = new System.Drawing.Size(242, 23);
            this.employers.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(104, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(110, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Age";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(110, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "City";
            // 
            // relationshipStatuses
            // 
            this.relationshipStatuses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.relationshipStatuses.FormattingEnabled = true;
            this.relationshipStatuses.Location = new System.Drawing.Point(251, 139);
            this.relationshipStatuses.Name = "relationshipStatuses";
            this.relationshipStatuses.Size = new System.Drawing.Size(242, 63);
            this.relationshipStatuses.TabIndex = 3;
            this.relationshipStatuses.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.relationshipStatuses_ItemCheck);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(95, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Employer";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(71, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Relationship status";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(89, 232);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "City section";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(82, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 10;
            this.label7.Text = "Smoking habit";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(83, 370);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 12;
            this.label8.Text = "Drinking habit";
            // 
            // button1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.button1, 2);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(490, 65);
            this.button1.TabIndex = 13;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // cities
            // 
            this.cities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cities.FormattingEnabled = true;
            this.cities.Location = new System.Drawing.Point(251, 71);
            this.cities.Name = "cities";
            this.cities.Size = new System.Drawing.Size(242, 23);
            this.cities.TabIndex = 14;
            // 
            // age
            // 
            this.age.Dock = System.Windows.Forms.DockStyle.Fill;
            this.age.Location = new System.Drawing.Point(251, 37);
            this.age.Name = "age";
            this.age.Size = new System.Drawing.Size(242, 23);
            this.age.TabIndex = 15;
            // 
            // name
            // 
            this.name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name.Location = new System.Drawing.Point(251, 3);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(242, 23);
            this.name.TabIndex = 16;
            // 
            // citySections
            // 
            this.citySections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.citySections.FormattingEnabled = true;
            this.citySections.ItemHeight = 15;
            this.citySections.Location = new System.Drawing.Point(251, 208);
            this.citySections.Name = "citySections";
            this.citySections.Size = new System.Drawing.Size(242, 63);
            this.citySections.TabIndex = 20;
            // 
            // smokingHabits
            // 
            this.smokingHabits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.smokingHabits.FormattingEnabled = true;
            this.smokingHabits.Location = new System.Drawing.Point(251, 277);
            this.smokingHabits.Name = "smokingHabits";
            this.smokingHabits.Size = new System.Drawing.Size(242, 63);
            this.smokingHabits.TabIndex = 21;
            this.smokingHabits.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.smokingHabits_ItemCheck);
            // 
            // drinkingHabits
            // 
            this.drinkingHabits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.drinkingHabits.FormattingEnabled = true;
            this.drinkingHabits.Location = new System.Drawing.Point(251, 346);
            this.drinkingHabits.Name = "drinkingHabits";
            this.drinkingHabits.Size = new System.Drawing.Size(242, 63);
            this.drinkingHabits.TabIndex = 22;
            this.drinkingHabits.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.drinkingHabits_ItemCheck);
            // 
            // NewCustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 483);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "NewCustomerForm";
            this.Text = "Registration";
            this.SizeChanged += new System.EventHandler(this.NewCustomerForm_SizeChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.age)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox relationshipStatuses;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox employers;
        private System.Windows.Forms.ComboBox cities;
        private System.Windows.Forms.NumericUpDown age;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.ListBox citySections;
        private System.Windows.Forms.CheckedListBox smokingHabits;
        private System.Windows.Forms.CheckedListBox drinkingHabits;
    }
}