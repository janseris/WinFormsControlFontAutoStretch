﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using JanSeris.FontAutoStretch.Demo.Model;
using JanSeris.FontAutoStretch.Demo.DataSource;
using JanSeris.FontAutoStretch.Source;

namespace JanSeris.FontAutoStretch.Demo
{
    public partial class NewCustomerForm : Form
    {
        public NewCustomerForm()
        {
            InitializeComponent();
            cities.Items.AddRange(DummyData.Cities);
            employers.Items.AddRange(DummyData.Employers);
            relationshipStatuses.Items.AddRange(Enum.GetValues(typeof(RelationshipStatus)).Cast<object>().ToArray());
            citySections.Items.AddRange(Enum.GetValues(typeof(Directions)).Cast<object>().ToArray());
            smokingHabits.Items.AddRange(Enum.GetValues(typeof(RelationshipStatus)).Cast<object>().ToArray());
            drinkingHabits.Items.AddRange(Enum.GetValues(typeof(RelationshipStatus)).Cast<object>().ToArray());
        }

        private Directions GetSelected()
        {
            Directions firstChecked = (Directions)relationshipStatuses.CheckedItems[0];
            return firstChecked;
        }

        private void EnsureExclusiveSelection(CheckedListBox list, ItemCheckEventArgs e, ItemCheckEventHandler itemCheckHandler)
        {
            if (e.NewValue == CheckState.Checked && list.CheckedItems.Count > 0)
            {
                list.ItemCheck -= itemCheckHandler;
                list.SetItemChecked(list.CheckedIndices[0], false);
                list.ItemCheck += itemCheckHandler;
            }
        }

        private void AutoChangeAllLabelsFontHeight()
        {
            var tableLayout = this.tableLayoutPanel1;
            var labels = tableLayout.Controls.OfType<Label>();
            float min = float.MaxValue;
            foreach(var label in labels)
            {
                SizeF rectangle = FontAutoStretchLibrary.GetTableLayoutRectangleSize(tableLayout, label);
                float newFontSize = FontAutoStretchLibrary.GetAutoFontSize(label, rectangle, linear: false);
                if(newFontSize < min)
                {
                    min = newFontSize;
                }
            }
            foreach(var label in labels)
            {
                label.Font = FontAutoStretchLibrary.GetResizedFont(label.Font, min);
            }
        }

        /*
        public static void StretchLabelFontInsideTableLayout(Form form, Label label, TableLayoutPanel tableLayout)
        {
            SizeF rectangle = FontAutoStretch.GetTableLayoutRectangleSize(tableLayout, label);
            FontAutoStretch.AutoChangeFontSize(label, rectangle, linear: false);
        }
        */

        private void AutoChangeButtonFontSize()
        {
            SizeF rectangle = FontAutoStretchLibrary.GetTableLayoutRectangleSize(this.tableLayoutPanel1, button1);
            FontAutoStretchLibrary.AutoChangeFontSize(button1, rectangle, linear: false);
        }

        private void NewCustomerForm_SizeChanged(object sender, EventArgs e)
        {
            AutoChangeAllLabelsFontHeight();
            AutoChangeButtonFontSize();
        }

        private void relationshipStatuses_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            EnsureExclusiveSelection(sender as CheckedListBox, e, relationshipStatuses_ItemCheck);
        }

        private void smokingHabits_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            EnsureExclusiveSelection(sender as CheckedListBox, e, smokingHabits_ItemCheck);

        }

        private void drinkingHabits_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            EnsureExclusiveSelection(sender as CheckedListBox, e, drinkingHabits_ItemCheck);
        }
    }
}
