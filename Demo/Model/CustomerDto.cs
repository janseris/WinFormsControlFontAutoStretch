﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JanSeris.FontAutoStretch.Demo.Model
{
    public class CustomerDto
    {
        public string Name { get; set; } = string.Empty;
        public int Age { get; set; }
        public RelationshipStatus RelationshipStatus { get; set; }
        public string City { get; set; }
        public Directions CitySection { get; set; }
        public string Employer { get; set; }
        public UsageFrequency SmokingHabit { get; set; }
        public UsageFrequency DrinkingHabit { get; set; }
    }
}
