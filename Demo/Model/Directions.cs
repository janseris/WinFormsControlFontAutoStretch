﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace JanSeris.FontAutoStretch.Demo.Model
{
    public enum Directions
    {
        North,
        Northeast,
        East,
        Southeast,
        South,
        Southwest,
        West,
        Northwest
    }
}
