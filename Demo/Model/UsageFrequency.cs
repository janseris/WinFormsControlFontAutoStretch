﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JanSeris.FontAutoStretch.Demo.Model
{
    public enum UsageFrequency
    {
        Never,
        Sometimes,
        Often
    }
}
