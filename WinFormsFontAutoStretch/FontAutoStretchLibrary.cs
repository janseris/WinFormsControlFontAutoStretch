﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace JanSeris.FontAutoStretch.Source
{
    /// <summary>
    /// Result for fitting label text into a large rectangle
    /// LINEAR: width: 127 iters in 3,789 ms
    /// EXP: width: 13 iters in 1,246 ms
    /// LINEAR: height: 34 iters in 1,588 ms
    /// EXP: height: 10 iters in 0,707 ms
    /// calculated font size by width: linear 71,5 exp 72,87317, by height: linear 25, exp 27,22168
    /// </summary>
    public class FontAutoStretchLibrary
    {

        /// <summary>
        /// Used for labels inside table layout panel control.
        /// </summary>
        /// <param name="form"></param>
        /// <param name="label"></param>
        /// <param name="tableLayout"></param>
        public static void StretchLabelFontInsideTableLayout(Form form, Label label, TableLayoutPanel tableLayout)
        {
            SizeF rectangle = FontAutoStretchLibrary.GetTableLayoutRectangleSize(tableLayout, label);
            FontAutoStretchLibrary.AutoChangeFontSize(label, rectangle, linear: false);
        }

        /// <summary>
        /// Changes <paramref name="control"/> font size so it fills the <paramref name="rectangle"/>
        /// <br><paramref name="rectangleMultiplier"/> is used to set the portion of the rectangle to fit in</br>
        /// <br><paramref name="linear"/> declares if the size growth is linear. If not, exponential growth is used (faster)</br>
        /// </summary>
        /// <param name="minFontSize">declares the lower limit of the autocalculated font size</param>
        /// <returns></returns>
        public static void AutoChangeFontSize(Control control, SizeF rectangle, float rectangleMultiplier = 0.7f, bool linear = true, float minFontSize = 8f)
        {/*
            float newFontSizeByWidth;
            float newFontSizeByHeight;
            float widthBound = rectangle.Width * rectangleMultiplier;
            float heightBound = rectangle.Height * rectangleMultiplier;

            float newFontSizeByWidthLinear;
            float newFontSizeByHeightLinear;
            float newFontSizeByWidthExponential; 
            float newFontSizeByHeightExponential;

            float newFontSize;
            if (linear)
            {
                newFontSizeByWidthLinear = AutoSizeFontByWidthLinear(control, widthBound, minFontSize);
                newFontSizeByHeightLinear = AutoSizeFontByHeightLinear(control, heightBound, minFontSize);
                newFontSize = Math.Min(newFontSizeByWidthLinear, newFontSizeByHeightLinear);
            }
            else
            {
                newFontSizeByWidthExponential = AutoSizeFontByWidthExponential(control, widthBound, minFontSize);
                newFontSizeByHeightExponential = AutoSizeFontByHeightExponential(control, heightBound, minFontSize);
                newFontSize = Math.Min(newFontSizeByWidthExponential, newFontSizeByHeightExponential);
            }

            //Trace.WriteLine(String.Format("calculated font size by width: linear {0} exp {1}, by height: linear {2}, exp {3}",
            //newFontSizeByWidthLinear, newFontSizeByWidthExponential, newFontSizeByHeightLinear, newFontSizeByHeightExponential));
            */
            float newFontSize = GetAutoFontSize(control, rectangle, rectangleMultiplier, linear, minFontSize);
            control.Font = GetResizedFont(control.Font, newFontSize);
        }


        /// <summary>
        /// Gets <paramref name="control"/> font size so it fills the <paramref name="rectangle"/>
        /// <br><paramref name="rectangleMultiplier"/> is used to set the portion of the rectangle to fit in</br>
        /// <br><paramref name="linear"/> declares if the size growth is linear. If not, exponential growth is used (faster)</br>
        /// </summary>
        /// <param name="minFontSize">declares the lower limit of the autocalculated font size</param>
        /// <returns></returns>
        public static float GetAutoFontSize(Control control, SizeF rectangle, float rectangleMultiplier = 0.7f, bool linear = true, float minFontSize = 8f)
        {
            float newFontSizeByWidth;
            float newFontSizeByHeight;
            float widthBound = rectangle.Width * rectangleMultiplier;
            float heightBound = rectangle.Height * rectangleMultiplier;

            float newFontSizeByWidthLinear;
            float newFontSizeByHeightLinear;
            float newFontSizeByWidthExponential;
            float newFontSizeByHeightExponential;

            float newFontSize;
            if (linear)
            {
                newFontSizeByWidthLinear = AutoSizeFontByWidthLinear(control, widthBound, minFontSize);
                newFontSizeByHeightLinear = AutoSizeFontByHeightLinear(control, heightBound, minFontSize);
                newFontSize = Math.Min(newFontSizeByWidthLinear, newFontSizeByHeightLinear);
            }
            else
            {
                newFontSizeByWidthExponential = AutoSizeFontByWidthExponential(control, widthBound, minFontSize);
                newFontSizeByHeightExponential = AutoSizeFontByHeightExponential(control, heightBound, minFontSize);
                newFontSize = Math.Min(newFontSizeByWidthExponential, newFontSizeByHeightExponential);
            }

            //Trace.WriteLine(String.Format("calculated font size by width: linear {0} exp {1}, by height: linear {2}, exp {3}",
            //newFontSizeByWidthLinear, newFontSizeByWidthExponential, newFontSizeByHeightLinear, newFontSizeByHeightExponential));

            return newFontSize;
        }

        private static float GetIthSize(float minSize, float baseIncrement, int n, float exp = 1.5f)
        {
            return minSize + baseIncrement * (float)Math.Pow(exp, n);
        }
        private static float AutoSizeFontByWidthExponential(Control control, float widthBound, float minFontSize)
        {
            //Stopwatch s = new Stopwatch();
            //s.Start();

            Font font = control.Font;
            string text = control.Text;
            float increment = 0.5f;
            float newFontSize = minFontSize;
            Font newFont = GetResizedFont(font, newFontSize);

            int iterations = 1;

            while (GetTextWidth(text, newFont) < widthBound)
            {
                newFontSize = GetIthSize(minFontSize, increment, iterations);
                newFont = GetResizedFont(newFont, newFontSize);
                iterations++;
            }

            //s.Stop();
            //Trace.WriteLine(String.Format("EXP: width: {0} iters in {1} ms", iterations, s.ElapsedMillisecondsFloat()));

            return newFontSize;
        }

        private static float AutoSizeFontByHeightExponential(Control control, float heightBound, float minFontSize)
        {
            //Stopwatch s = new Stopwatch();
            //s.Start();

            Font font = control.Font;
            string text = control.Text;
            float increment = 0.5f;
            float newFontSize = minFontSize;
            Font newFont = GetResizedFont(font, newFontSize);


            int iterations = 1;

            while (GetTextHeight(text, newFont) < heightBound)
            {
                newFontSize = GetIthSize(minFontSize, increment, iterations);
                newFont = GetResizedFont(newFont, newFontSize);
                iterations++;
            }

            //s.Stop();
            //Trace.WriteLine(String.Format("EXP: height: {0} iters in {1} ms", iterations, s.ElapsedMillisecondsFloat()));
            return newFontSize;
        }


        //35 iterations per ms
        private static float AutoSizeFontByWidthLinear(Control control, float widthBound, float minFontSize)
        {
            //Stopwatch s = new Stopwatch();
            //s.Start();

            Font font = control.Font;
            string text = control.Text;
            float increment = 0.5f;
            float newFontSize = minFontSize;
            Font newFont = GetResizedFont(font, newFontSize);

            int iterations = 0;
            while (GetTextWidth(text, newFont) < widthBound)
            {
                newFontSize += increment;
                newFont = GetResizedFont(newFont, newFontSize);
                iterations++;
            }

            //s.Stop();
            //Trace.WriteLine(String.Format("LINEAR: width: {0} iters in {1} ms", iterations, s.ElapsedMillisecondsFloat()));
            return newFontSize;
        }

        //35 iterations per ms
        private static float AutoSizeFontByHeightLinear(Control control, float heightBound, float minFontSize)
        {
            //Stopwatch s = new Stopwatch();
            //s.Start();

            Font font = control.Font;
            string text = control.Text;
            float increment = 0.5f;
            float newFontSize = minFontSize;
            Font newFont = GetResizedFont(font, newFontSize);

            int iterations = 0;
            while (GetTextHeight(text, newFont) < heightBound)
            {
                newFontSize += increment;
                newFont = GetResizedFont(newFont, newFontSize);
                iterations++;
            }

            //s.Stop();
            //Trace.WriteLine(String.Format("LINEAR: height: {0} iters in {1} ms", iterations, s.ElapsedMillisecondsFloat()));
            return newFontSize;
        }


        private static SizeF GetTextDimensions(string text, Font font)
        {
            using (Graphics g = Graphics.FromHwnd(IntPtr.Zero)) //dummy graphics object
            {
                SizeF size = g.MeasureString(text, font);
                return size;
            }
        }

        private static float GetTextHeight(string text, Font font)
        {
            return GetTextDimensions(text, font).Height;
        }

        private static float GetTextWidth(string text, Font font)
        {
            return GetTextDimensions(text, font).Width;
        }

        public static Font GetResizedFont(Font f, float newSize)
        {
            var newFont = new Font(f.Name, newSize);
            return newFont;
        }

        /// <summary>
        /// Gets rectangle size of the <paramref name="tableLayout"/> cell the <paramref name="control"/> is in
        /// </summary>
        /// <returns></returns>
        public static SizeF GetTableLayoutRectangleSize(TableLayoutPanel tableLayout, Control control)
        {
            TableLayoutPanelCellPosition pos = tableLayout.GetCellPosition(control);
            int width = tableLayout.GetColumnWidths()[pos.Column];
            int height = tableLayout.GetRowHeights()[pos.Row];
            return new SizeF(width, height);
        }

    }

    public static class StopWatchExtensions
    {
        public static long ElapsedMicroSeconds(this Stopwatch watch)
        {
            return watch.ElapsedTicks * 1000000 / Stopwatch.Frequency;
        }

        public static string ElapsedMillisecondsFloat(this Stopwatch watch)
        {
            return String.Format("{0:F3}", watch.ElapsedMicroSeconds() / 1000d);
        }
    }
}
